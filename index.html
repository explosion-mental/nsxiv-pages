<!DOCTYPE html>
<html lang="en">
<head>
<title>nsxiv - Neo Simple X Image Viewer</title>
<link rel="stylesheet" href="style.css"><link rel="icon" type="image/png" href="https://raw.githubusercontent.com/nsxiv/nsxiv/master/icon/16x16.png" sizes="16x16"></head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width">
<base href="https://codeberg.org/nsxiv/nsxiv/src/branch/master/" target="_self">
</head>
<body>
<p><a href="https://codeberg.org/nsxiv/nsxiv"><img src="https://codeberg.org/nsxiv/pages/raw/branch/master/img/logo.png" alt="nsxiv" /></a></p>

<p><a href="https://codeberg.org/nsxiv/nsxiv"><img src="https://img.shields.io/badge/Hosted_at-Codeberg-%232185D0?style=flat-square&amp;logo=CodeBerg" alt="CodeBerg" /></a>
<a href="https://codeberg.org/nsxiv/nsxiv/tags"><img src="https://img.shields.io/github/v/tag/nsxiv/nsxiv?style=flat-square" alt="tags" /></a>
<a href="https://codeberg.org/nsxiv/nsxiv/src/branch/master/LICENSE"><img src="https://img.shields.io/badge/license-GPL--2.0-lightgreen?style=flat-square" alt="license" /></a>
<a href="https://codeberg.org/nsxiv/nsxiv"><img src="https://img.shields.io/tokei/lines/github/nsxiv/nsxiv?color=red&amp;style=flat-square" alt="loc" /></a></p>

<p><a href="https://nsxiv.codeberg.page/man/">View Man Page</a></p>
<p><a href="https://nsxiv.codeberg.page/changelog/">View Changelog</a></p>
<h2 id="neo-or-new-or-not-simple-or-small-or-suckless-x-image-viewer"><strong>Neo (or New or Not) Simple (or Small or Suckless) X Image Viewer</strong></h2>

<p>nsxiv is a fork of the now-unmaintained <a href="https://github.com/muennich/sxiv">sxiv</a>
with the purpose of being a (mostly) drop-in replacement for sxiv, maintaining its
interface and adding simple, sensible features. nsxiv is free software licensed
under GPLv2 and aims to be easy to modify and customize.</p>

<p>Please file a bug report if something does not work as documented or expected on
<a href="https://codeberg.org/nsxiv/nsxiv/issues/new">Codeberg</a> after making sure you are using the latest release.  Contributions
are welcome, see <a href="CONTRIBUTING.md#contribution-guideline">CONTRIBUTING.md</a> to get started.</p>

<h2 id="features">Features</h2>

<ul>
<li>Basic image operations like zooming, panning, rotating</li>
<li>Basic support for animated&#47;multi-frame images</li>
<li>Thumbnail mode: grid of selectable previews of all images</li>
<li>Ability to cache thumbnails for fast re-loading</li>
<li>Automatically refreshing modified images</li>
<li>Customizable keyboard and mouse mappings via <code>config.h</code></li>
<li>Scriptability via <code>key-handler</code></li>
<li>Displaying image information in status bar via <code>image-info</code> &#38; <code>thumb-info</code></li>
<li>Customizable window title via <code>win-title</code></li>
</ul>

<h2 id="screenshots">Screenshots</h2>

<p><strong>Image mode with default colors:</strong></p>

<p><img src="https://codeberg.org/nsxiv/pages/raw/branch/master/img/image.png" alt="Image" title="Image mode" /></p>

<p><strong>Thumbnail mode with custom colors:</strong></p>

<p><img src="https://codeberg.org/nsxiv/pages/raw/branch/master/img/thumb.png" alt="Thumb" title="Thumb mode" /></p>

<h2 id="installing-via-package-manager">Installing via package manager</h2>

<p><a href="https://repology.org/project/nsxiv/versions">
  <img align="right" width="192" src="https://repology.org/badge/vertical-allrepos/nsxiv.svg">
</a></p>

<p>nsxiv is available on the following distributions&#47;repositories. If you don&#8217;t see
your distro listed here, either contact your distro&#8217;s package maintainer or
consider packaging it yourself and adding it to the respective community repo.</p>

<p>Repos not tracked by repology:</p>

<ul>
<li>Fedora: Enable the copr repo via <code>dnf copr enable mamg22&#47;nsxiv</code>.</li>
</ul>

<h2 id="dependencies">Dependencies</h2>

<p>nsxiv requires the following software to be installed:</p>

<ul>
<li>Imlib2</li>
<li>X11</li>
</ul>

<p>The following dependencies are optional.</p>

<ul>
<li><code>inotify</code><sup>ℹ</sup>: Used for auto-reloading images on change.
Disabled via <code>HAVE_INOTIFY=0</code>.</li>
<li><code>libXft</code>, <code>freetype2</code>, <code>fontconfig</code>: Used for the status bar.
Disabled via <code>HAVE_LIBFONTS=0</code>.</li>
<li><code>giflib</code>: Used for animated gif playback.
Disabled via <code>HAVE_LIBGIF=0</code>.</li>
<li><code>libexif</code>: Used for auto-orientation and exif thumbnails.
Disable via <code>HAVE_LIBEXIF=0</code>.</li>
<li><code>libwebp</code>: Used for animated webp playback.
(<strong><em>NOTE</em></strong>: animated webp also requires Imlib2 v1.7.5 or above)
Disabled via <code>HAVE_LIBWEBP=0</code>.</li>
</ul>

<p>Please make sure to install the corresponding development packages in case that
you want to build nsxiv on a distribution with separate runtime and development
packages (e.g. *-dev on Debian).</p>

<p>[ℹ]: <a href="https://www.man7.org/linux/man-pages/man7/inotify.7.html">inotify</a> is a Linux-specific API for monitoring filesystem changes.
  It&#8217;s not natively available on <code>*BSD</code> systems but can be enabed via installing
  and linking against <a href="https://github.com/libinotify-kqueue/libinotify-kqueue">libinotify-kqueue</a>.</p>

<h2 id="building">Building</h2>

<p>nsxiv is built using the commands:</p>

<pre><code>$ make
</code></pre>

<p>You can pass <code>HAVE_X=0</code> to <code>make</code> to disable an optional dependency.
For example:</p>

<pre><code>$ make HAVE_LIBEXIF=0
</code></pre>

<p>will disable <code>libexif</code> support. Alternatively they can be disabled via editing
<code>config.mk</code>. <code>OPT_DEP_DEFAULT=0</code> can be used to disable all optional
dependencies.</p>

<p>Installing nsxiv:</p>

<pre><code># make install
</code></pre>

<p>Installing desktop entry:</p>

<pre><code># make install-desktop
</code></pre>

<p>Installing icons:</p>

<pre><code># make install-icon
</code></pre>

<p>Installing all of the above:</p>

<pre><code># make install-all
</code></pre>

<p>Please note, that these requires root privileges.
By default, nsxiv is installed using the prefix <code>&#47;usr&#47;local</code>, so the full path
of the executable will be <code>&#47;usr&#47;local&#47;bin&#47;nsxiv</code>, the <code>.desktop</code> entry will be
<code>&#47;usr&#47;local&#47;share&#47;applications&#47;nsxiv.desktop</code> and the icon path will be
<code>&#47;usr&#47;local&#47;share&#47;icons&#47;hicolor&#47;{size}&#47;apps&#47;nsxiv.png</code>.</p>

<p>You can install nsxiv into a directory of your choice by changing this command to:</p>

<pre><code>$ make PREFIX="&#47;your&#47;dir" install
</code></pre>

<p>Example scripts are installed using <code>EGPREFIX</code> which defaults to
<code>&#47;usr&#47;local&#47;share&#47;doc&#47;nsxiv&#47;examples</code>. You can change <code>EGPREFIX</code> the same way
you can change <code>PREFIX</code> shown above.</p>

<p>The build-time specific settings of nsxiv can be found in the file <em>config.h</em>.
Please check and change them, so that they fit your needs.
If the file <em>config.h</em> does not already exist, then you have to create it with
the following command:</p>

<pre><code>$ make config.h
</code></pre>

<h2 id="usage">Usage</h2>

<p>Refer to the man page for the documentation:</p>

<pre><code>$ man nsxiv
</code></pre>

<h2 id="f.a.q.">F.A.Q.</h2>

<ul>
<li><p>Can I open remote urls with nsxiv? <br>
Yes, see <a href="https://codeberg.org/nsxiv/nsxiv-extra/src/branch/master/scripts/nsxiv-url">nsxiv-url</a></p></li>
<li><p>Can I open all the images in a directory? <br>
Yes, see <a href="https://codeberg.org/nsxiv/nsxiv-extra/src/branch/master/scripts/nsxiv-rifle">nsxiv-rifle</a></p></li>
<li><p>Can I set default arguments for nsxiv? <br>
Yes, see <a href="https://codeberg.org/nsxiv/nsxiv-extra/src/branch/master/scripts/nsxiv-env">nsxiv-env</a></p></li>
<li><p>Can I pipe images into nsxiv? <br>
Yes, see <a href="https://codeberg.org/nsxiv/nsxiv-extra/src/branch/master/scripts/nsxiv-pipe">nsxiv-pipe</a></p></li>
</ul>

<p>You may also wish to see the <a href="https://codeberg.org/nsxiv/nsxiv/issues/242">known issues</a>.</p>

<h2 id="customization">Customization</h2>

<p>The main method of customizing nsxiv is by setting values for the variables in <em>config.h</em>,
or by using Xresources as explained in the manual. If these options are not sufficient,
you may implement your own features by following
<a href="https://codeberg.org/nsxiv/nsxiv-extra/blob/master/CUSTOMIZATION.md">this guide</a>.</p>

<p>Due to our limited <a href="CONTRIBUTING.md#project-scope">project scope</a>, certain features or
customization cannot be merged into nsxiv mainline. Following the spirit of suckless
software, we host the <a href="https://codeberg.org/nsxiv/nsxiv-extra">nsxiv-extra</a> repo where users
are free to submit whatever patches or scripts they wish.</p>

<p>If you think your custom features can be beneficial for the general user base and is within
our project scope, please submit it as a pull request on this repository, then we <em>may</em>
merge it to mainline.</p>

<p>Description on how to use or submit patches can be found on
nsxiv-extra&#8217;s <a href="https://codeberg.org/nsxiv/nsxiv-extra">README</a>.</p>

<h2 id="download">Download</h2>

<p>You can <a href="https://codeberg.org/nsxiv/nsxiv">browse</a> the source code repository
on CodeBerg or get a copy using git with the following command:</p>

<pre><code>$ git clone https:&#47;&#47;codeberg.org&#47;nsxiv&#47;nsxiv.git
</code></pre>

<p>You can view the changelog <a href="CHANGELOG.md">here</a></p>

<h2 id="similar-projects">Similar projects</h2>

<p>If nsxiv isn&#8217;t able to fit your needs, check out the image viewer section of
<strong><a href="https://suckless.org/rocks">suckless rocks</a></strong> to find other minimal image
viewers to try out.</p>

<p>Below are a couple other lesser known projects not listed in suckless rocks.</p>

<ul>
<li><a href="https://github.com/explosion-mental/mage">MAGE</a>:
A smaller&#47;more-suckless version of sxiv.</li>
<li><a href="https://github.com/TAAPArthur/div">div</a>:
Minimal and extensive, aimed at C devs willing to build their own features.</li>
<li><a href="https://github.com/occivink/mpv-image-viewer">mpv-image-viewer</a>:
Lua script to turn mpv into an image viewer. Supports thumbnails via
<a href="https://github.com/occivink/mpv-gallery-view">mpv-gallery-view</a>.</li>
</ul>
</body>
</html>
