#!/bin/sh

# Dependencies: cat, lowdown, groff

# location to the nsxiv repo at disk.
# use `nsxiv_repo="/path/to/repo" ./update.sh` to override the default
nsxiv_repo="${nsxiv_repo:-..}"

if ! [ "$(cd $nsxiv_repo && git branch --show-current)" = "master" ]; then
  echo "nsxiv repo is not at master branch" >&2
  exit 1
fi

# Mainpage
cat template/main.html > index.html
lowdown --html-no-skiphtml --html-no-escapehtml < "$nsxiv_repo/README.md" \
  | sed -f template/main.sed >> index.html
cat template/template-end-body.html >> index.html

# Manpage
css='<link rel="stylesheet" href="../style.css">'
icon_src='https://raw.githubusercontent.com/nsxiv/nsxiv/master/icon/16x16.png'
icon='<link rel="icon" type="image/png" href="'"$icon_src"'" sizes="16x16">'

groff -mandoc -Thtml < "$nsxiv_repo/nsxiv.1" | sed 's|^</head>|'"${css}${icon}"'</head>|' > man/index.html

# Changelog
cat template/change.html > changelog/index.html
lowdown --html-no-skiphtml --html-no-escapehtml < "$nsxiv_repo/CHANGELOG.md" >> changelog/index.html
cat template/template-end-body.html >> changelog/index.html
